#Github Training

#An Introduction
**Snapshots & Distribution** 

Git creates a set of snapshots of a filesystem. Every commit is like taking a snapshot of every file, every state, and every change. Git is different from subversion because subversion is a list of _file based changes_ which are made to each file over a period of time. 

**Distributed**
* Operations are local so you can view the repo history locally instead of reaching out to a server 
*  There is no single point of failure
* Git uses a sha-1 hash for before anything is stored
* Anything that is stored is stored by its sha-1 hash 

Since Git takes a snapshot of the filesystem you can still work with it even though the central server is not available. The client repositories can be copied back and the repo can be recreated easily since you can continue to commit while offline. 

###The three stages of Git
**Committed**: Stored in your local database

**Modified**: The file is changed but not committed. 

**Staged**: A modified file has been marked to go into your next commit snapshot. 

The three stages of git may look like this in your workflow 

1. Modify a file (git rm, vim, etc.)

2. Stage the file (git commit -m “message”)

3. Commit the file (git push)

##Set Up
**Mac**
```brew install git```

Don’t have brew? Get it.


**Linux**

```apt-get|yum|pacman|zypper install git```


**Windows**

Download the x64 git executeable from [Here](https://github.com/git-for-windows/git/releases/download/v2.10.0.windows.1/Git-2.10.0-64-bit.exe)
Download the x32 git executeable from [Here](https://github.com/git-for-windows/git/releases/download/v2.10.0.windows.1/Git-2.10.0-32-bit.exe)

For a free GUI client I'd recommend [Github Desktop](https://desktop.github.com/) which gives you a nice visual representation of your repositories. If you'd like something with more functionality I would suggest Atlassian's [SourceTree](https://www.sourcetreeapp.com/). 

You can use git in Powershell and [this link](https://git-scm.com/book/en/v2/Git-in-Other-Environments-Git-in-Powershell) will show you how. 

test

##Configuring Git

From the command line you'll want to use the `git config` command to set up your environment. 

```
git config --global user.name "Firstname Lastname"

$ git config --global user.email firstnamelastname@domain.com
```

This will let us know who made all those terrible commits. 


Set up your global editor with `git config --global core.editor vim`.
If you really want to use emacs, I've heard that it's a good operating system which comes with a decent text editor. 


Check your settings with `git config --list`. 
You can customize your `/etc/gitconfig` 

**Example**

```
# Core variables
[core]
        ; Don't trust file modes
        filemode = false
# Our diff algorithm
[diff]
        external = /usr/local/bin/diff-wrapper
        renames = true
[branch "devel"]
        remote = origin
        merge = refs/heads/devel
# Proxy settings
[core]
        gitProxy="ssh" for "kernel.org"
        gitProxy=default-proxy ; for the rest
[include]
        path = /path/to/foo.inc ; include by absolute path
        path = foo ; expand "foo" relative to the current file
        path = ~/foo ; expand "foo" in your $HOME directory
Values
```


#Workflow

##Creating A New Repo


From your local command line, start with a README file. 

```echo "# git_training" >> README.md```


Then initialize your directory with git

```git init```


Remember the 3 stages of git? Your README file has been created but not yet added to the snapshot, so you'll have to add the file you've created. 

```git add README.md```


Now we have to stage the file by creating a commit message. Try to be verbose and to the point. 

```git commit -m "This is my first commit"```


Now you need to add a new remote repo, as opposed to your local repo, named _origin_. 

```git remote add origin git@github.body.prod:mark-white/git_training.git```


This allows you to push and pull to and from _github.body.prod:markk-white/git_training.git_ by using _origin_ instead of the full url. The repository must already exist for this to make sense.  

At this point you could check the status of your local repository with `git status`.

Now you can push to the origin. This command is basically saying, "Push the changes in this folder on this local branch of _master_ to my remote branch named _origin_. 

```git push -u origin master```


##Cloning A Repository

Let's say that we want a co-worker to edit our README file and fix some errors.
Your co-worker would need to _clone_ a copy of your repo to their local environment. The repository would have to be public or your co-worker would need to be added as either an owner or collaborator. 

Clone by the url

```git clone http://github.body.prod/mark-white/git_training```

----------

#File Manipulation

##Tracking & Staging
`git add` will track a file. When you add a file and run `git status`, you will see that the file is now under the section __Changes to be commited__. 
* When you commit, the state of the file at the time you added it will be included in the snapshot
* If the state of the file changes before you commit, you will have to re-track the file with `git add`
* `git diff` will provide you with more detail about your local repo and let you know what exactly has changed since you started tracking. 

##Removing Files
`git rm` will remove files and `git rm -rf` will remove directories. Commiting will remove the file or directory from the remote repository. 

`git rm --cached` can be used to remove a file from your staging area but keep it in your working tree. It works much like your `~/.gitignore` file in that it stops tracking the removed file. 

`git mv` works the same as the linux `mv` command. 

##Restoring Files

Oh no, I accidentally deleted testfile2. In order to restore that file you'll want to restore your origin to a state prior to the deleted file. Use `git log` to find the commit previous to the deletion and restore origin with `git checkout c23c1b9fe8510cc011a3d41e582b1db13f5202f4 .`

You can see this workflow in the repository history. 

##Unstaging Files
`git reset HEAD <filename>` will unstage a file if you don't want to include it in your next commit. 
If you made changes to a file and you want to revert it to before you made changes you can use `git checkout -- <filename>`.

#Commits

##Commit History

The commit history is useful for tracking who did what to the files in your repository. If you clone an existing repository you'll also pull down the commit history for the repo. 

The latest commit will be at the top along with it's SHA-1 checksum, and commiters information. This is when it becomes important to set your email address and username in with 

```git config --global user.name "Firstname Lastname"```

```git config --global user.email firstnamelastname@domain.com```

One of the most useful options for `git log` is `-p`, which will show you the difference between each commit. This makes it easier to spot an issue and fix it. 

#Tags

Tags can be very useful in that they can be used to version files in your repository. We should try to use annotated tags as opposed to lightweight tags. 

To tag your most recent commit with a version you can use `git tag -a v1.0 -m 'Version uno'`. To view the tag information you would run `git show v1.0` and get something similar to this:

```
[02:42:27] Mark.White@BBCOMs-MacBook-Pro:~/Repos/git_training> git show v1.0
tag v1.0
Tagger: Mark White <mark.white@bodybuilding.com>
Date:   Tue Jul 28 14:39:59 2015 -0700

Version uno

commit 1a0ac9d5a2ad31ac29c3931a2c9a06f8fef45535
Author: Mark White <mark.white@bodybuilding.com>
Date:   Tue Jul 28 14:27:02 2015 -0700

    pushing

diff --git a/README.md b/README.md
index 7f68633..febcada 100644
--- a/README.md
+++ b/README.md
@@ -157,13 +157,18 @@ Clone by the url
```

 `git mv` works the same as the linux `mv` command.

###Tagging Previous Commits

If you need to retro tag something you may have missed you can show your commit history with hashes using `git log --pretty = oneline`. You can then use the hash to retroactively tag the commit with `git tag -a v0.1 930f88641ccd349e60ccfb2f7ee29726661ccd98`. 

Then you can do a `git show v0.1` to verify. 

Keep in mind that a push will not send your tags. You have to tell `git push` to use your tag with `git push origin v1.1`. 
```
Counting objects: 1, done.
Writing objects: 100% (1/1), 172 bytes | 0 bytes/s, done.
Total 1 (delta 0), reused 0 (delta 0)
To git@github.body.prod:mark-white/git_training.git
 * [new tag]         v1.1 -> v1.1
 ```

I added a new file named *testfile2* and tagged my repo with version v1.2. I made a push with this new version and now when I visit the repo on github.body.prod I will be able to switch between tags from the branch/tag pulldown. You will see that if you switch from v1.1 to v1.2, the file *testfile2* is now present. 

###Checking Out Tags

If you'd like to pull down a specific tag from the repository you'll have to create a new branch and specify the tag you'd like to pull. For example:

```git checkout -b version1.0_branch v1.0```


#Aliases

Git aliases work on the same concept as bash aliases in that you can use a short from of a command. For example, you could alias checkout to simply __ci__. 
```git config --global alias.ci commit```

Another useful alias to find the last commit in the repository is `git config --global alias.last 'log -1 HEAD'`. Now I can see my last commit by typing in `git last`:

```
commit fe70939829278e49f12d529ef1e427408ad3f3ab
Author: Mark White <mark.white@bodybuilding.com>
Date:   Tue Jul 28 15:13:03 2015 -0700

    commiting before pulling down a tag
```

#Branching

Branching is good if you want to prevent a lot of commits to the master branch or if you need to work on something outside of the master branch until that something is complete. The master branch is not a special branch but is created with the default name of __master__ when you run a `git init`.

When you __commit__ your files/code your repository creates a pointer to the root tree with all of the commit metadata. If you make more changes and make another commit then it creates a new pointer to the commit which came before it. This enables us to restore data from a previous commit and allows for branching because a branch is just a moveable pointer. 


In the __git_training__ repo I'll create a new branch named *my_first_branch*. This creates a new pointer at the same commit you’re currently on.
```git branch my_first_branch```

Now we switch to the new branch with `git checkout my_first_branch` and verify that we are on the new branch with `git branch`. 

* If you make a change and commit it then the __my_first_branch__ branch will be ahead of master by one commit.  

You can switch back to master and you will see that the data in master is preserved. I created a new file in the master branch and named it *branchFile*. If I checkout my_first_branch, I will not see that file. 

For a visual representation of what has occured you can run `git log --oneline --decorate --graph --all` which will output something similar to this:
```
* c6f5ae1 (master) adding a branch test file
| * b48eae5 (HEAD, my_first_branch) updated readme for my_first_branch
|/
* 1c65cc0 (origin/master) udpated README
* fe70939 commiting before pulling down a tag
* 4bedbe3 Updated Readme
* 86116f3 (tag: v1.2) added testfile2
* 942e74f udpated readme
* 546cdfd (tag: v1.1, version1.1) added a testfile
* 1a0ac9d (tag: v1.0, version1.0) pushing
* 44c2a30 stuff
* 5344a36 stuff
*   1a736b6 Merge branch 'master' of github.body.prod:mark-white/git_training
|\
| *   4dfab2e Merge branch 'master' of github.body.prod:mark-white/git_training
| |\
* | \   41fe8fa Merge branch 'master' of github.body.prod:mark-white/git_training amending some stuff Conflicts:        README.md
|\ \ \
| |/ /
|/| /
| |/
| * 31b2aca added intermediate info
* | 2e853bf amending stuff
* | dccec62 doing an ammendment
|/
* 7ffd1c9 updated
* 930f886 (tag: v0.1) fixed something?
* b931647 added more info
* 4438007 fixed spacing
* e8a4ca5 updated README
* c37f3e8 udpated README
* 2b16766 another commit
* 61836dd 1st commit
```

When you're finished making changes on the __my_first_branch__ you'll want to commit those changes and `git checkout master`. Resolve any conflicts that come up and merge your branch with master by using `git merge my_first_branch`. 
When you look at the log you'll see that we merged the branch into master:

```
*   266b015 (HEAD, origin/master, master) Merge branch 'my_first_branch'
|\
| * 35332dc (my_first_branch) adding updated readme
| * b48eae5 updated readme for my_first_branch
* | c6f5ae1 adding a branch test file
|/
* 1c65cc0 udpated README
```

You may run into merge conflicts from time to time. Use `git status` to find the source of these conflicts. 

It's good practice to remove branches that you are no longer working on. If you need to create "hotfix" branches in the future you would want to create a new branch each time. Remove your old branch with `git branch -d my_first_branch`. The branch will still be present for training purposes. If you have a long running branch that you find useful for features or long running changes, then keep that branch around and use it within your development cycle. 

#Rebasing

When you merge a branch with another branch git will do a 3 way merge between the two latest branch snapshots and the most recent common ancestor. A rebase uses the common ancestor of two branches to take all of the changes that were committed on one branch and apply them to another. 

We may not be doing a rebase workflow since it's more difficult to perform conflict resolution. If you need to rebase, go ahead, but try to stick with merging for now. 

#Pull Requests

We want to be able to review changes that could have an impact on our more sensitive systems. It'd be a good idea to create a local branch to develop on and then create a pull request to have your changes pulled into the master branch. 

That workflow would look something like this:

1. Create a local branch `git checkout -b testbranch`
2. Add a file or modify an existing file. 
3. `git commit` the file and push `git push origin testbranch`
4. Go to repo on github.body.prod and click on the *branch* dropdown for the repo. 
5. Select _testbranch_ and click "Compare & pull request"
6. Fill out some info and "Create pull request"
7. In the screen that comes up you can view commit info, the changes that will take place, and the ability to merge the pull request. 
8. We should have at least 2 people review the pull request before the submitter is allowed to merge the pull request. I have commented on the pull request to simulate this. 
9. When ready, you can click on "Merge pull request" and then "Confirm merge"
10. You will have the option to delete the branch. You will typically delete short term branches but keep long term branches such as a dev, qa, prod, stage branch. The _testbranch_ will be kept for now for training purposes. 

#Links
[Pro Git Book](https://git-scm.com/book/en/v2)
